---
title: Eeasy Frontend
---

# Welcome to Easy Frontend

When many people work on the same frontend codebase, there's always discussion happening. It might be around technology, how to write the best specs, which best practices should be used. Discussions like this should sound familiar:

- Should we use this pattern or should we use that one?
- Can we use this bleeding edge feature?
- This is my catch-all-forver-solution, wdyt?

Because of these questions, I decided to write this easy frontend guide.
While I'll add some best practice guides for topics like [Vue](/vue/) or [testing](/testing/) this section is a generic one aiming for pragmatism, explicitness and stability.

# Make Pragmatic Decisions

- what makes a pragmatic decision
- choose the simple path
- you are not alone
- be smart, but don't try to outsmart others

## Premature optimizations

- Signs of premature optimizations
- Why you should not go down that rout

# Simple Architecture

## Consistency

## Avoid Globals

## Avoid Magic Functions


