module.exports = {
  title: "Easy Frontend",
  description: 'Playing around',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guides', items: [
        { text: 'Vue', link: '/vue/'},
        { text: 'Testing', link: '/testing/'}
      ]}
    ],
    sidebar: "auto"
  }
}
